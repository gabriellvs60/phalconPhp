<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class UsuarioImcController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for usuario_imc
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'UsuarioImc', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $usuario_imc = UsuarioImc::find($parameters);
        if (count($usuario_imc) == 0) {
            $this->flash->notice("The search did not find any usuario_imc");

            $this->dispatcher->forward([
                "controller" => "usuario_imc",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $usuario_imc,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function initialAction()
    {
        $this->view->pick('usuario_imc/new');
    }

    /**
     * Edits a usuario_imc
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $usuario_imc = UsuarioImc::findFirstByid($id);
            if (!$usuario_imc) {
                $this->flash->error("usuario_imc was not found");

                $this->dispatcher->forward([
                    'controller' => "usuario_imc",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $usuario_imc->id;

            $this->tag->setDefault("id", $usuario_imc->id);
            $this->tag->setDefault("nome", $usuario_imc->nome);
            $this->tag->setDefault("email", $usuario_imc->email);
            $this->tag->setDefault("idade", $usuario_imc->idade);
            $this->tag->setDefault("altura", $usuario_imc->altura);
            $this->tag->setDefault("peso", $usuario_imc->peso);
            
        }
    }

    /**
     * Creates a new usuario_imc
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "usuario_imc",
                'action' => 'index'
            ]);

            return;
        }

        $usuario_imc = new UsuarioImc();
        $usuario_imc->nome = $this->request->getPost("nome");
        $usuario_imc->email = $this->request->getPost("email", "email");
        $usuario_imc->idade = $this->request->getPost("idade");
        $usuario_imc->altura = $this->request->getPost("altura");
        $usuario_imc->peso = $this->request->getPost("peso");
        

        if (!$usuario_imc->save()) {
            foreach ($usuario_imc->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "usuario_imc",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("usuario_imc was created successfully");

        $this->dispatcher->forward([
            'controller' => "usuario_imc",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a usuario_imc edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "usuario_imc",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $usuario_imc = UsuarioImc::findFirstByid($id);

        if (!$usuario_imc) {
            $this->flash->error("usuario_imc does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "usuario_imc",
                'action' => 'index'
            ]);

            return;
        }

        $usuario_imc->nome = $this->request->getPost("nome");
        $usuario_imc->email = $this->request->getPost("email", "email");
        $usuario_imc->idade = $this->request->getPost("idade");
        $usuario_imc->altura = $this->request->getPost("altura");
        $usuario_imc->peso = $this->request->getPost("peso");
        

        if (!$usuario_imc->save()) {

            foreach ($usuario_imc->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "usuario_imc",
                'action' => 'edit',
                'params' => [$usuario_imc->id]
            ]);

            return;
        }

        $this->flash->success("usuario_imc was updated successfully");

        $this->dispatcher->forward([
            'controller' => "usuario_imc",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a usuario_imc
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $usuario_imc = UsuarioImc::findFirstByid($id);
        if (!$usuario_imc) {
            $this->flash->error("usuario_imc was not found");

            $this->dispatcher->forward([
                'controller' => "usuario_imc",
                'action' => 'index'
            ]);

            return;
        }

        if (!$usuario_imc->delete()) {

            foreach ($usuario_imc->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "usuario_imc",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("usuario_imc was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "usuario_imc",
            'action' => "index"
        ]);
    }

}
