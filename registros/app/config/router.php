<?php

$router = $di->getRouter();

// Define your routes here
$router->add('/usuarioimc/initial', ['controller' => 'usuarioimc', 'action' => 'initial']);

$router->handle();
